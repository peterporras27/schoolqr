<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Log;
use QrCode;
use Carbon\Carbon;

class ApiController extends Controller
{
    // https://www.simplesoftware.io/docs/simple-qrcode#docs-usage
    public function qr( $size = 100, $format = 'png', $key )
    {

    	$size = preg_replace('/\D/', '', $size );
    	$size = ( empty( $size ) ) ? 100: $size;
    	$formats = array('png','eps','svg');
    	$format = ( in_array($format, $formats) ) ? $format:'png';
        return QrCode::format( $format )->size( $size )->generate( $key );
    }

    public function school_card( $id ){

        $member = User::find( $id );

        if ( !$member ) {
            return redirect('/');
        }

        return view( 'layouts.id', ['member' => $member ] );
    }

    public function log_update( Request $request )
    {
        $response = array(
            'error' => false,
            'message' => 'failed',
            'data' => '',
        );

        $log = Log::find($request->input('log_id'));

        if ($log) {

            $log->purpose = $request->input('purpose');
            $log->save();

            $response['message'] = 'Success!';
        }

        return response()->json( $response );
    }

    public function api_log( Request $request )
    {
        $response = array(
            'error' => false,
            'visitor' => false,
            'visitor_id' => '',
            'log_id' => '',
            'message' => '',
            'data' => '',
        );

        // Check if event key exist
        if ( !$request->input('id') ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: member not found.';
            return response()->json( $response );
        }

        // Find user who owns specific hash code.
        $member = User::find($request->input('id'));

        // Check if member exist.
        if ( ! $member ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: member not found.';
            return response()->json( $response );
        }

        // Check if login type
        if ( !$request->input('log_type') ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Select login type.';
            return response()->json( $response );
        }

        $logtype = strtolower( $request->input('log_type') );

        $logTypes = array('login','logout');
        if ( !in_array( $request->input('log_type'), $logTypes ) ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Select login type.';
            return response()->json( $response );
        }

        // check if user already logged in for today

        $lastRecord = Log::where('user_id', '=', $member->id)->latest()->first();

        $name = $member->first_name . ' ' . $member->last_name;
        $msg = '';

        $hasRcord = false;
        if ($lastRecord) {
            if ($lastRecord->log_type == $logtype) {
                $hasRcord = true;
            }
        }
        
        if ( $hasRcord ) {

            if ( $logtype=='logout' ) {
                $msg = $name .' already Logged out.';
            } else {
                $msg = $name . ' already Logged in.';
            }

        } else {

            $log = new Log();
            $log->log_type = $logtype; // login, logout, onetime
            $log->purpose = '';
            $log->user_id = $member->id;
            $log->save();

            if ($member->visitor) {
                $response['visitor'] = true;
                $response['visitor_id'] = $member->id;
                $response['log_id'] = $log->id;
            }

            $msg = 'SUCCESS: '.$name . ' logged in!';

            if ( $logtype=='logout' ) {
                $msg = 'SUCCESS: '. $name .' Logged out!';
            }
        }

        $response['log_type'] = $logtype;
        $response['data'] = $member;
        $response['message'] = $msg;
        
        return response()->json( $response );
    }
}
