<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">

            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <span>
                        <img alt="image" class="img-circle" src="{{asset('img/profile_small.jpg')}}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span> 
                            <span class="text-muted text-xs block">Options <b class="caret"></b></span> 
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ url('settings') }}">Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">JMO</div>
            </li>

            <li>
                <a href="{{ route('home') }}"> 
                    <i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span>
                </a>
            </li>
    
            @if ( Auth::user()->hasRole('admin') )
            <li>
                <a href="{{ route('users.index') }}"> 
                    <i class="fa fa-users"></i> <span class="nav-label">Users</span>
                </a>
            </li>
            <li>
                <a href="{{ route('roles.index') }}"> 
                    <i class="fa fa-lock"></i> <span class="nav-label">User Roles</span>
                </a>
            </li>
            @endif

            <li>
                <a href="{{ route('settings') }}">
                    <i class="fa fa-cogs"></i> <span class="nav-label">Profile Settings</span>
                </a>
            </li>

        </ul>
    </div>
</nav>