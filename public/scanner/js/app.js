// const container = document.querySelector(".contents")
var type = 'login';
var visitor_id = '';
var log_id = '';

const showScanned = () => {

    let output = ""
    // container.innerHTML = output
    var html5QrcodeScanner = new Html5QrcodeScanner(
        "qr-reader", { fps: 10, qrbox: 250 }
    );

    html5QrcodeScanner.render(onScanSuccess);
}

document.addEventListener("DOMContentLoaded", showScanned)

function onScanSuccess(decodedText, decodedResult) 
{
    // console.log(`Code scanned = ${decodedText}`, decodedResult);   

    type= jQuery('[name="type"]:checked').val();

    jQuery.ajax({
        url: '/api/log',
        type: 'GET',
        dataType: 'json',
        data: {id: decodedText,log_type:type},
    }).always(function(e) {
        
        console.log(e);

        if (e.error) {

            $('#info').html(e.message);

        } else {

            var html = '';
            if (e.log_type == 'login') {

                html += '<table class="table table-striped"><thead>';
                var fname = (e.data.first_name) ? e.data.first_name:'';
                var lname = (e.data.last_name) ? e.data.last_name:'';
                var vehicle_type = (e.data.vehicle_type) ? e.data.vehicle_type:'';
                var plate_number = (e.data.plate_number) ? e.data.plate_number:'';
                var vehicle_model = (e.data.vehicle_model) ? e.data.vehicle_model:'';

                html += '<tr>';
                html += '<th scope="col">Data</th>';
                html += '<th scope="col">Information</th>';
                html += '</tr>';
                html += '<tbody>';
                html += '<tr><td>Name:</td><td>'+fname+' '+lname+'</td></tr>';
                html += '<tr><td>Vehicle Type:</td><td>'+vehicle_type+'</td></tr>';
                html += '<tr><td>Plate Number:</td><td>'+plate_number+'</td></tr>';
                html += '<tr><td>Vehicle Model:</td><td>'+vehicle_model+'</td></tr>';

                if (e.visitor) {

                    visitor_id = e.visitor_id;
                    log_id = e.log_id;

                    $('#purpose').show();

                    if (e.data.school) {html += '<tr><td>Department:</td><td>'+e.data.school+'</td></tr>';}
                    if (e.data.id_number) {html += '<tr><td>ID Number:</td><td>'+e.data.id_number+'</td></tr>';}
                    if (e.data.notify_person) {html += '<tr><td>Incase of Emergency Notify:</td><td>'+e.data.notify_person+'</td></tr>';}
                    if (e.data.notify_address) {html += '<tr><td>Address:</td><td>'+e.data.notify_address+'</td></tr>';}
                    if (e.data.notify_phone) {html += '<tr><td>Phone:</td><td>'+e.data.notify_phone+'</td></tr>';}
                }
                
                html += '</tbody></table>';

            } else {

                html += e.message;
            }

            jQuery('#info').html(html);
        }

        jQuery('#purposeModal').modal('show');
    });
}

function sendPurpose()
{
    jQuery('#purposeModal').modal('hide');

    $('#info').html('');
    $('#purpose').hide();

    jQuery.ajax({
        url: '/api/log/update',
        type: 'GET',
        dataType: 'json',
        data: {visitor_id: visitor_id, log_id:log_id, purpose: jQuery('#purpose textarea').val()},
    }).always(function(e) {
        
        console.log(e);
    });
}

if ("serviceWorker" in navigator) {
    window.addEventListener("load", function() {
        navigator.serviceWorker
        .register("/serviceWorker.js")
        .then(res => console.log("service worker registered"))
        .catch(err => console.log("service worker not registered", err))
    })
}