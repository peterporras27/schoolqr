<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>West Visayas State University Pototan Campus</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 30px;
                font-family: none;
                margin-right:15px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            @media(max-width: 767px ) {
                .left-side{display: none;}
                .title{font-size: 1em; margin-right:0;}
                .content{margin-top: 100px;}
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="left-side" style="width: 73%; background:url('{{ asset('img/pototan.jpg') }}') no-repeat center center; height: 100%; background-size: cover;"></div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        @if ( Auth::user()->hasRole('admin') )
                            <a href="{{ route('home') }}">Dashboard</a>
                            <a href="{{ route('users.index') }}">Users</a>
                        @endif
                        <a href="{{ route('settings') }}">Profile Settings</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                </div>
            @endif
            <br>
            <div class="content">
                @if (Auth::check())
                    <div class="title m-b-md">
                        West Visayas State University<br> Pototan Campus
                    </div>
                    <div align="center">
                        <img style="width: 300px;" src="{{ route('qr',[400,'png',Auth::user()->id]) }}" alt="" class="img-responsive">
                        <h1>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</h1>
                    </div>
                @else
                    <div class="title m-b-md">
                        West Visayas State University<br> Pototan Campus
                    </div>
                    <div class="links"></div>
                @endif
            </div>
        </div>
    </body>
</html>
