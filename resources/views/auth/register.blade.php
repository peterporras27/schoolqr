@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12"> <!-- col-md-offset-2 -->
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">

                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="first_name" class="col-md-5 control-label">Are you a visitor?</label>
                                    <div class="col-md-6">
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="visitor" value="yes" id="option1" checked> Yes
                                            </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="visitor" value="no" id="option2"> No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name" class="col-md-5 control-label">First Name</label>
                                    <div class="col-md-6">
                                        <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name" class="col-md-5 control-label">Last Name</label>
                                    <div class="col-md-6">
                                        <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                    <label for="middle_name" class="col-md-5 control-label">Middle Name</label>
                                    <div class="col-md-6">
                                        <input id="middle_name" type="text" class="form-control" name="middle_name" value="{{ old('middle_name') }}" required autofocus>
                                        @if ($errors->has('middle_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('middle_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="others" style="display: none;">

                                    <div class="form-group{{ $errors->has('id_number') ? ' has-error' : '' }}">
                                        <label for="id_number" class="col-md-5 control-label">ID Number:</label>
                                        <div class="col-md-6">
                                            <input id="id_number" type="text" class="form-control" name="id_number" value="{{ old('id_number') }}">
                                            @if ($errors->has('id_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('id_number') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-5 control-label">E-Mail Address</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-5 control-label">Password</label>
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-5 control-label">Confirm Password</label>
                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                
                                <div class="form-group{{ $errors->has('vehicle_type') ? ' has-error' : '' }}">
                                    <label for="first_name" class="col-md-5 control-label">Vehicle type:</label>
                                    <div class="col-md-6">
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="vehicle_type" value="Car"> Car
                                            </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="vehicle_type" value="Motorcycle"{{ old('vehicle_type') == 'Motorcycle' ? ' checked':'' }}> Motorcycle
                                            </label>
                                        </div>
                                        @if ($errors->has('vehicle_type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('vehicle_type') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('vehicle_brand') ? ' has-error' : '' }}">
                                    <label for="vehicle_brand" class="col-md-5 control-label">Vehicle Brand:</label>
                                    <div class="col-md-6">
                                        <input type="hidden" name="vehicle_brand" value="">
                                        <select id="vehicle_brand" type="text" class="form-control">
                                        </select>
                                        @if ($errors->has('vehicle_brand'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('vehicle_brand') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('vehicle_model') ? ' has-error' : '' }}">
                                    <label for="vehicle_model" class="col-md-5 control-label">Vehicle Model:</label>
                                    <div class="col-md-6">
                                        <input type="hidden" name="vehicle_model" value="">
                                        <select id="vehicle_model" type="text" class="form-control">
                                        </select>
                                        @if ($errors->has('vehicle_model'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('vehicle_model') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('vehicle_color') ? ' has-error' : '' }}">
                                    <label for="vehicle_color" class="col-md-5 control-label">Vehicle Color:</label>
                                    <div class="col-md-6">
                                        <input id="vehicle_color" type="text" class="form-control" name="vehicle_color" placeholder="Black" value="{{ old('vehicle_color') }}" required>
                                        @if ($errors->has('vehicle_color'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('vehicle_color') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                                    <label for="plate_number" class="col-md-5 control-label">Plate Number:</label>
                                    <div class="col-md-6">
                                        <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ old('plate_number') }}" required>
                                        @if ($errors->has('plate_number'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('plate_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="others" style="display: none;">
                                    
                                    <div class="form-group{{ $errors->has('school') ? ' has-error' : '' }}">
                                        <label for="school" class="col-md-5 control-label">School/Department:</label>
                                        <div class="col-md-6">
                                            <input id="school" type="text" class="form-control" name="school" value="{{ old('school') }}" placeholder="School of information...">
                                            @if ($errors->has('school'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('school') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('notify_person') ? ' has-error' : '' }}">
                                        <label for="notify_person" class="col-md-5 control-label">Incase of Emergency Notify:</label>
                                        <div class="col-md-6">
                                            <input id="notify_person" type="text" class="form-control" name="notify_person" value="{{ old('notify_person') }}" placeholder="Name...">
                                            @if ($errors->has('notify_person'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('notify_person') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('notify_address') ? ' has-error' : '' }}">
                                        <label for="notify_address" class="col-md-5 control-label">Address:</label>
                                        <div class="col-md-6">
                                            <input id="notify_address" type="text" class="form-control" name="notify_address" value="{{ old('notify_address') }}" placeholder="Brgy. ...">
                                            @if ($errors->has('notify_address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('notify_address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('notify_phone') ? ' has-error' : '' }}">
                                        <label for="notify_phone" class="col-md-5 control-label">Phone:</label>
                                        <div class="col-md-6">
                                            <input id="notify_phone" type="number" class="form-control" name="notify_phone" value="{{ old('notify_phone') }}" placeholder="0909...">
                                            @if ($errors->has('notify_phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('notify_phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-lg">
                                    Register
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
jQuery(document).ready(function($) {
    $('[name="visitor"]').change(function(){
        var isVisitor = $(this).val();

        if ( isVisitor == 'yes' ) {
            $('.others').fadeOut();
        } else {
            $('.others').fadeIn();
        }
    });

    loadCars();

    $('#vehicle_brand').change(function() {

        var type = $('[name="vehicle_type"]:checked').val();
        var brand = $(this).val();
        $('[name="vehicle_brand"]').val(brand);

        if (type=="Motorcycle") {

            getMotors(brand);

        } else {

            $.ajax({
                url: 'car-list.json',
                type: 'GET',
                dataType: 'json',
                data: {},
            }).always(function(e) {

                var op = '<option value="">Select Model</option>';
                for (var i = e.length - 1; i >= 0; i--) {
                    if (e[i].brand == brand) {
                        for (var j = 0; j < e[i].models.length; j++) {
                            op += '<option value="'+e[i].models[j]+'">'+e[i].models[j]+'</option>';
                        }
                    }
                }
                $('#vehicle_model').html(op);
            });
        }
    });
    

    $('[name="vehicle_type"]').change(function() {
        
        var type = $(this).val();

        $('#vehicle_brand').html('<option value="">Select Brand</option>');
        $('#vehicle_model').html('<option value="">Select Model</option>');
        $('[name="vehicle_brand"]').val('');
        $('[name="vehicle_model"]').val('');

        if (type=="Motorcycle") {

            loadMotors();

        } else {

            loadCars();
        }
    });

    $('#vehicle_model').change(function() {
        $('[name="vehicle_model"]').val($(this).val());
    });
});

function loadMotors(){

    $.ajax({
        url: 'moto_brands.json',
        type: 'GET',
        dataType: 'json',
        data: {},
    }).always(function(e) {

        var op = '<option value="">Select Brand</option>';
        for (var i = e.data.length - 1; i >= 0; i--) {
            op += '<option value="'+e.data[i].id+'">'+e.data[i].name+'</option>';
        }
        
        $('#vehicle_brand').html(op);
    });
}

function getMotors(id){

    $.ajax({
        url: 'moto_models.json',
        type: 'GET',
        dataType: 'json',
        data: {},
    }).always(function(e) {

        var op = '<option value="">Select Model</option>';
        for (var i = e.data.length - 1; i >= 0; i--) {
            if (e.data[i].brand_id==id) {
                op += '<option value="'+e.data[i].name+'">'+e.data[i].name+'</option>';
            }
        }

        $('#vehicle_model').html(op);
    });
    
}

function loadCars(){

    $.ajax({
        url: 'car-list.json',
        type: 'GET',
        dataType: 'json',
        data: {},
    }).always(function(e) {
        
        var op = '<option value="">Select Brand</option>';
        var mod = '<option value="">Select Model</option>';
        for (var i = e.length - 1; i >= 0; i--) {
            op += '<option value="'+e[i].brand+'">'+e[i].brand+'</option>';
            for (var j = 0; j < e[i].models.length; j++) {
                mod += '<option value="'+e[i].models[j]+'">'+e[i].models[j]+'</option>';
            }
        }

        $('#vehicle_brand').html(op);
        $('#vehicle_model').html(mod);
    });
}
</script>
@endsection