@extends('admin')

@section('content')

<form action="{{ route('profile.update') }}" method="POST" role="form">
{{ csrf_field() }}

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
			
			<div class="panel-heading">
				<div class="panel-title">User Information <small class="m-l-sm">Edit your account information.</small></div>
				<div class="panel-options"></div>
			</div>
			
			<!-- panel body -->
			<div class="panel-body">

				<div class="form-group">
					<label for="first_name">First Name</label>
					<input type="text" value="{{ Auth::user()->first_name }}" name="first_name" class="form-control" id="first_name" placeholder="first name">
				</div>

				<div class="form-group">
					<label for="last_name">Last Name</label>
					<input type="text" value="{{ Auth::user()->last_name }}" name="last_name" class="form-control" id="last_name" placeholder="last name">
				</div>

				<div class="form-group">
					<label for="middle_name">Middle Name</label>
					<input type="text" value="{{ Auth::user()->middle_name }}" name="middle_name" class="form-control" id="middle_name" placeholder="middle name">
				</div>

				@if(!Auth::user()->visitor)
				<div class="form-group">
					<label for="school">School</label>
					<input type="text" value="{{ Auth::user()->school }}" name="school" class="form-control" id="school" placeholder="School of Information & Communications Technology">
				</div>

				<div class="form-group">
					<label for="id_number">ID Number</label>
					<input type="text" value="{{ Auth::user()->id_number }}" name="id_number" class="form-control" id="id_number" placeholder="">
				</div>

				<div class="form-group">
					<label for="notify_person">Incase Of Emergency Notify</label>
					<input type="text" value="{{ Auth::user()->notify_person }}" name="notify_person" class="form-control" id="notify_person" placeholder="">
				</div>

				<div class="form-group">
					<label for="notify_address">Address</label>
					<input type="text" value="{{ Auth::user()->notify_address }}" name="notify_address" class="form-control" id="notify_address" placeholder="">
				</div>

				<div class="form-group">
					<label for="notify_phone">Phone Number</label>
					<input type="text" value="{{ Auth::user()->notify_phone }}" name="notify_phone" class="form-control" id="notify_phone" placeholder="">
				</div>
				@endif

				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" value="{{ Auth::user()->email }}" name="email" class="form-control" id="email" placeholder="email">
				</div>
			
				<button type="submit" class="btn btn-primary btn-icon">Save <i class="fa fa-save"></i></button>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
			<div class="panel-heading">
				<div class="panel-title">Vehicle Details</div>
				<div class="panel-options"></div>
			</div>
			<div class="panel-body">
				
				<div class="form-group{{ $errors->has('vehicle_type') ? ' has-error' : '' }}">
                    <label for="first_name" class=" control-label">Vehicle type:</label>
                    <div class="">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="vehicle_type" value="Car"{{ Auth::user()->vehicle_type == 'Car' ? ' checked':'' }}> Car
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="vehicle_type" value="Motorcycle"{{ Auth::user()->vehicle_type == 'Motorcycles' ? ' checked':'' }}> Motorcycle
                            </label>
                        </div>
                        @if ($errors->has('vehicle_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('vehicle_type') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('vehicle_model') ? ' has-error' : '' }}">
                    <label for="vehicle_model" class=" control-label">Vehicle Model:</label>
                    <div class="">
                        <input id="vehicle_model" type="text" class="form-control" name="vehicle_model" value="{{ Auth::user()->vehicle_model }}" placeholder="Toyota Wigo..." required>
                        @if ($errors->has('vehicle_model'))
                            <span class="help-block">
                                <strong>{{ $errors->first('vehicle_model') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                    <label for="plate_number" class=" control-label">Plate Number:</label>
                    <div class="">
                        <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ Auth::user()->plate_number }}" required>
                        @if ($errors->has('plate_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('plate_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

			</div>
		</div>
	</div>

</div>

</form>

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
			
			<div class="panel-heading">
				<div class="panel-title">Change Password <small class="m-l-sm">Use a minimum of 6 letter, number, & symbol combinations.</small></div>
				
				<div class="panel-options"></div>
			</div>
			
			<!-- panel body -->
			<div class="panel-body">
				<form action="{{ route('password.update') }}" method="POST" role="form">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="old_password">Old Password</label>
						<input type="password" name="old_password" class="form-control" id="old_password" placeholder="Old password">
					</div>

					<div class="form-group">
						<label for="new_password">New Password</label>
						<input type="password" name="password" class="form-control" id="new_password" placeholder="New password">
					</div>

					<div class="form-group">
						<label for="password_confirmation">Confrim Password</label>
						<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password confirmation">
					</div>
				
					<button type="submit" class="btn btn-primary btn-icon">Update <i class="fa fa-save"></i></button>
				</form>
			</div>
		</div>
	</div>	
</div>


@endsection