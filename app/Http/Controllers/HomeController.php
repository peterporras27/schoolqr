<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;
use App\User;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'Dashboard',
            'description' => 'Deafault landing page for admin',
        );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;

        $this->params['member_count'] = User::count();
        $this->params['logs_count'] = Log::count();
        $this->params['logs'] = Log::paginate( $show );
        $this->params['perpage'] = $show;

        return view('members.logs', $this->params);
    }

    public function print_report( Request $request )
    {
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        
        $start = date( 'Y-m-d', strtotime( $request->input('start_date') ) );
        $end = date( 'Y-m-d', strtotime( $request->input('end_date') ) );

        $this->params['start'] = '';
        $this->params['end'] = '';
        
        if ( isset( $start ) && isset( $end ) ) {

            $this->params['logs'] = Log::paginate( $show );

        } else {

            $this->params['logs'] = Log::whereBetween('created_at', [$start, $end])->paginate( $show );
            $this->params['start'] = date( 'F d, Y', strtotime( $request->input('start_date') ) );
            $this->params['end'] = date( 'F d, Y', strtotime( $request->input('end_date') ) );
        }

        return view('layouts.print', $this->params);           
    }
}
