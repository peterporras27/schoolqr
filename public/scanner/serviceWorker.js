const staticSchoolQR = "dev-school-qr-v1"
const assets = [
  "scanner/",
  "scanner/index.html",
  "scanner/css/bootstrap.min.css.map",
  "scanner/css/bootstrap.min.css",
  "scanner/css/style.css",
  "scanner/js/jquery-3.6.0.min.js",
  "scanner/js/bootstrap.min.js",
  "scanner/js/html5-qrcode.min.js",
  "scanner/js/app.js",
]

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(staticSchoolQR).then(cache => {
      cache.addAll(assets)
    })
  )
})

self.addEventListener("fetch", fetchEvent => {
  fetchEvent.respondWith(
    caches.match(fetchEvent.request).then(res => {
      return res || fetch(fetchEvent.request)
    })
  )
})