<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');	
});

// Mobile API
Route::get('api/log', 'ApiController@api_log')->name('api_log');
Route::get('api/log/update', 'ApiController@log_update')->name('log_update');
Route::get('qr/{size}/{format}/{key}', 'ApiController@qr')->name('qr');
Route::get('card/{id}', 'ApiController@school_card')->name('card');

Auth::routes();

// admin routes
Route::get('dashboard', 'HomeController@index')->name('home');
Route::get('print', 'HomeController@print_report')->name('print');

// settings
Route::get('settings', 'SettingsController@index')->name('settings');
Route::post('settings/profile', 'SettingsController@profile_update')->name('profile.update');
Route::post('settings/password', 'SettingsController@password_update')->name('password.update');

// Restful Controllers
Route::resources([
	'users' => 'UsersController',
	'roles' => 'RolesController',
	'members' => 'MembersController',
]);

