<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	protected $fillable = [
        'purpose',
        'log_type',
        'user_id',
    ];
    
    public function user()
    {
      	return $this->hasOne(
            User::class, 
            'id', // foreign key
            'user_id' // local key
        )->first();
    }
}
