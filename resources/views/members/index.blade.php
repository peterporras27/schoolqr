@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">User List<small class="m-l-sm"> Manage all user accounts.</small></div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">

		<form action="{{ route('members.index') }}" method="GET" class="form-inline" role="form" style="direction: rtl;">

			<div class="input-group">
				<span class="input-group-addon">Per page</span> <input type="number" name="show" style="width: 80px;" placeholder="10" value="{{ $perpage }}" class="form-control">
			</div>

			<div class="input-group">
				<input type="text" name="search" value="{{ $search }}" placeholder="Name" class="form-control">
				<span class="input-group-btn"> 
					<button type="submit" class="btn btn-info" style="margin: 0;">Search <i class="fa fa-search"></i></button>
				</span>
			</div>

			<div class="input-group pull-right">
        		<a href="{{ route('members.index') }}" class="btn btn-white">
					<i class="fa fa-refresh"></i> Reset Search
				</a>
			</div>
		</form>
		<hr>
		
		@if( $members->count() )
			
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th><strong>First Name</strong></th>
						<th><strong>Last Name</strong></th>
						<th><strong>Middle Name</strong></th>
						<th><strong>Status</strong></th>
						<th><strong>Card Number</strong></th>
						<th><strong>Library Card</strong></th>
						<th><strong>Options</strong></th>
					</tr>
				</thead>
				<tbody>
					@foreach( $members as $member )

						<tr id="user-row-{{ $member->id }}">
							<td>{{ $member->first_name }}</td>
							<td>{{ $member->last_name }}</td>
							<td>{{ $member->middle_name }}</td>
							<td>{{ $member->active ? 'Active':'Inactive' }}</td>
							<td>{{ $member->card_number }}</td>
							<td>
								<button 
									data-photo="{{ $member->photo }}" 
									data-hash="{{ $member->card_number }}" 
									data-json="{{ json_encode($member) }}" 
									data-card="{{ url('card',$member->id) }}"
									class="btn btn-info btn-xs" 
									data-toggle="modal" 
									data-target="#user-qrcode">
									<i class="fa fa-qrcode"></i> View QR Code
								</button>
							</td>
							<td>
								{{-- <a href="{{ url("members/{$member->id}/edit") }}" class="btn btn-success btn-xs">
									<i class="fa fa-edit"></i> Edit
								</a> --}}
								
								<button data-id="{{ $member->id }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
									<i class="fa fa-close"></i> Remove
								</button>
								
							</td>
						</tr>

					@endforeach
				</tbody>
			</table>
		</div>
		{{ $members->appends( request()->input() )->links() }}
		@else
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>No data found,</strong> you may now start adding new members.
		</div>
		@endif
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Member</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, this user will no longer be able to login on his/her account and all data affiliated to this account will be deleted.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-right btn btn-white"  data-dismiss="modal">Cancel</button>

                <button type="button" id="remove-btn" class="btn btn-danger">Remove</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="user-qrcode" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Library Card</h4>
            </div>
            <div class="modal-body" style="text-align: left;">

            	<div class="idbox" style="float:left;width: 300px;border: 1px solid #ccc;margin-right:15px;">

            		<div class="banner" align="center" style="padding: 15px;background: #2076a5;color:#fff;font-family: sans-serif;">
            			<h4 style="margin: 0;">TOMAS CONFESOR MEMORIAL<BR>PUBLIC LIBRARY</h4>
            			<h5 style="margin: 0;">San Agustin St., Cabatuan, Iloilo</h5>
            		</div>

            		<div style="padding: 15px;">

            			<div style="float:left;width:49%;" align="center">sss
            				<img id="profile-img" src="" style="width:100%;">
            			</div>

            			<div style="float:left; width: 49%;" align="center">
            				<img id="qrcode-img" src="" style="width:100%;">
            			</div>
            			<div style="clear:both;"></div>
            			<div style="width:100%;margin-top: 0px;line-height: 1.8;font-size:13px;" align="center">
            				<div class="underline" id="card_number"></div>
            				<strong>ID No.</strong>
            				<div class="underline" id="fullname"></div>
            				<strong>Name</strong>
            				<div class="underline" id="cardaddress"></div>
            				<strong>Home Address</strong>
            				<div class="underline" id="phone_number"></div>
							<strong>Contact No.</strong>
            			</div>
            		</div>

            	</div>


            	<div class="idbox" style="float:left;width: 300px;border: 1px solid #ccc;">

            		<div class="banner" align="center" style="padding: 15px;background: #2076a5;color:#fff;font-family: sans-serif;">
            			<h4 style="margin: 0;">IMPORTANT</h4>
            		</div>

            		<div style="padding: 15px;">
            			<div style="width:100%;margin-top: 0px;line-height: 1.8;font-size:13px;" align="center">
            				<p>This Card is Personal Card, and the owner cannot lend it to anybody under penalty of losing his library privvilages.</p>
            				<p>The Owner is responsible for all the books issued under the name in serial number appearing on the face of this card.</p>
            				<p>Any tampering or defacing of this will render it null and void.</p>
            			</div>
            			<div class="date">{{ date('Y') }}</div>
            			<div class="date">{{ date('Y') + 1 }}</div>
            			<div class="date">{{ date('Y') + 2 }}</div>
            			<div class="date">{{ date('Y') + 3 }}</div>
            			<div style="clear:both;"></div>
            			<div align="center">
            				<h5 style="color:  #2076a5 !important;">ELIADORA A. PEDROSO</h5>
            				<div><i>Computer File Librarian II</i></div>
            				<br>
            				<h5 style="color:  #2076a5 !important;">EDUARDO S. TUARES</h5>
            				<div><i>Municipal Vide Mayor</i></div>
            			</div>
            		</div>

            	</div>
            	<div style="clear:both;"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white pull-right"  data-dismiss="modal">Close</button>
                <a href="#" id="card-link" class="btn btn-info" target="_blank">
                	<i class="fa fa-print"></i> Print
                </a>
            </div>
        </div>
    </div>
</div>

<div id="print-id" style="">
	
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('members.create') }}" class="btn btn-primary">Add Member <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<style>
.idbox .underline {
    border-bottom: 1px solid #2076a5;
    font-size: 18px;
    color:  #2076a5 !important;
}
.idbox .date{
    width:25%;
    text-align: center;
    padding:15px;
    border: 1px solid #2076a5;
    float:left;
    color:  #2076a5 !important;
    margin-bottom: 23px;
}

.idbox h5, .idbox h4{margin:0; color: #fff !important}
.idbox strong, .idbox p, .idbox i{color:  #2076a5 !important;}
</style>
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var user_id = button.data('id');
		jQuery('#remove-btn').data('id',user_id);
  		jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+user_id+' td:first').html() );
	});

	jQuery('#user-qrcode').on('show.bs.modal', function (event) {

		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var hash = button.data('hash');
		var card = button.data('card');
		var data = button.data('json');

		jQuery('#qrcode-img').attr('src', '{{ url('/') }}/qr/250/png/'+hash);
		jQuery('#profile-img').attr('src', '{{ url('/') }}/uploads/'+data.photo);

		
		// var html = '<img src="'+'{{ url('/') }}/qr/250/png/'+hash+'" style="margin-left:-15px;margin-top:-15px;" class="img-responsive">';
		jQuery('#card_number').html(data.card_number);
		jQuery('#fullname').html(data.first_name+' '+data.middle_name+' '+data.last_name);
		jQuery('#cardaddress').html(data.address);
		jQuery('#phone_number').html(data.phone_number);
  		
  		jQuery('#card-link').attr('href',card);
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var user_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('members.index') }}/'+user_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if (data.error) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+user_id).remove();
			}
		});
		 
	});
});
</script>
@endsection
