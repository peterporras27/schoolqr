<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $validate = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];

        if ( isset( $data['id_number'] ) ) 
        {
            $validate['id_number'] = 'required|string';
            // $validate['school'] = 'required|string';
            // $validate['notify_person'] = 'required|string';
            // $validate['notify_address'] = 'required|string';
            // $validate['notify_phone'] = 'required|string';
        }

        return Validator::make($data,$validate);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $details = [];
        
        $fields = [
            'first_name',
            'middle_name',
            'last_name',
            'email',
            'id_number',
            'school',
            'password',
            
            'notify_person',
            'notify_address',
            'notify_phone',

            'plate_number',
            'vehicle_type',
            'vehicle_brand',
            'vehicle_model',
            'vehicle_color'
        ];

        if ( $data['visitor'] == 'no' ) {
            $details['visitor'] = false;
        }

        foreach ($fields as $field) {
            if ( isset($data[$field]) ) {
                if (!empty($data[$field])) {
                    if ($field == 'password') {
                        $details[$field] = bcrypt($data[$field]);
                    } else {
                        $details[$field] = $data[$field];
                    }
                }   
            }
        }

        $user = User::create($details);

        $user->roles()->attach( Role::where('name', 'member')->first() );

        return $user;
    }
}
