<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->string('middle_name')->default('');

            $table->string('email')->unique();
            $table->string('password')->nullable();

            $table->enum('gender',['male','female'])->nullable();
            $table->text('school')->nullable();
            $table->string('id_number')->nullable();

            $table->string('notify_person')->nullable();
            $table->string('notify_address')->nullable();
            $table->string('notify_phone')->nullable();

            $table->string('plate_number')->nullable();
            $table->string('vehicle_type')->nullable();
            $table->string('vehicle_brand')->nullable();
            $table->string('vehicle_color')->nullable();
            $table->string('vehicle_model')->nullable();

            $table->boolean('visitor')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
