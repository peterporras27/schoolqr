<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>School QR</title>

	<link rel="icon" href="{{asset('assets/images/favicon.ico')}}">

	<link rel="stylesheet" href="{{asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/font-icons/entypo/css/entypo.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/font-icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-core.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-theme.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-forms.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">

	<script src="{{asset('assets/js/jquery-1.11.3.min.js')}}"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	@yield('styles')
	<style>
	th{text-align:left;}
	</style>

</head>
<body class="page-body" data-url="{{ url('/') }}">

	<div class="page-container{{ Auth::user()->hasRole('admin') ? '':' sidebar-collapsed' }}"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
		
		<div class="sidebar-menu">

			<div class="sidebar-menu-inner">
				
				<header class="logo-env">

					<!-- logo -->
					{{-- <div class="logo">
						<a href="#">
							<img src="{{asset('assets/images/logo@2x.png')}}" width="120" alt="" />
						</a>
					</div> --}}

					<!-- logo collapse icon -->
					<div class="sidebar-collapse">
						<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
							<i class="entypo-menu"></i>
						</a>
					</div>

									
					<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
					<div class="sidebar-mobile-menu visible-xs">
						<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
							<i class="entypo-menu"></i>
						</a>
					</div>

				</header>
				
										
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
		                <a href="{{ url('/') }}"> 
		                    <i class="fa fa-home"></i> <span class="nav-label">Home</span>
		                </a>
		            </li>
		            
					@if ( Auth::user()->hasRole('admin') )
					<li>
		                <a href="{{ route('home') }}"> 
		                    <i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span>
		                </a>
		            </li>
		    
		            
		            <li>
		                <a href="{{ route('users.index') }}"> 
		                    <i class="fa fa-users"></i> <span class="nav-label">Users</span>
		                </a>
		            </li>
		            {{-- <li>
		                <a href="{{ route('roles.index') }}"> 
		                    <i class="fa fa-lock"></i> <span class="nav-label">User Roles</span>
		                </a>
		            </li> --}}
		            @endif

		            <li>
		                <a href="{{ route('settings') }}">
		                    <i class="fa fa-cogs"></i> <span class="nav-label">Profile Settings</span>
		                </a>
		            </li>

				</ul>
				
			</div>

		</div>

		<div class="main-content">
			<div class="row">
		
				<!-- Profile Info and Notifications -->
				<div class="col-md-6 col-sm-8 clearfix">
			
					<ul class="user-info pull-left pull-none-xsm">
			
						<!-- Profile Info -->
						<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
			
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="{{asset('img/profile_small.jpg')}}" alt="" class="img-circle" width="44">
								{{ Auth::user()->name }}
							</a>
			
							<ul class="dropdown-menu">
			
								<!-- Reverse Caret -->
								<li class="caret"></li>
			
								<!-- Profile sub-links -->
			
								<li>
									<a href="{{ route('settings') }}">
					                    <i class="fa fa-cogs"></i> 
					                	Profile Settings
					                </a>
								</li>
			
								<li>
									<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
										<i class="entypo-logout right"></i>
										Log Out 
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			
				<!-- Raw Links -->
				<div class="col-md-6 col-sm-4 clearfix hidden-xs">
					<ul class="list-inline links-list pull-right">
						<li class="sep"></li>
						<li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
		                        Log Out <i class="entypo-logout right"></i>
		                    </a>

		                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		                        {{ csrf_field() }}
		                    </form>
						</li>
					</ul>
				</div>
			
			</div>
			
			<hr>
			
			
			@yield('action')
			

			
			<h2>{{ ( $title ) ? $title:'' }}</h2>
            <span>{{ ( $description ) ? $description:'' }}</span>
            
            <div class="row">
				@include('inc.alert')
			</div>
			<br>

			
			@yield('content')
			
            <div class="clearfix"></div>
            

			<footer class="main">&copy; {{ date('Y') }} <strong></strong> Admin Panel</footer>
		</div><!-- .main-content -->
	</div><!-- .page-container -->

	<!-- Bottom scripts (common) -->
	<script src="{{asset('assets/js/gsap/TweenMax.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
	<script src="{{asset('assets/js/bootstrap.js')}}"></script>
	<script src="{{asset('assets/js/joinable.js')}}"></script>
	<script src="{{asset('assets/js/resizeable.js')}}"></script>
	<script src="{{asset('assets/js/neon-api.js')}}"></script>

	<!-- JavaScripts initializations and stuff -->
	<script src="{{asset('assets/js/neon-custom.js')}}"></script>
	<!-- Demo Settings -->
	<script src="{{asset('assets/js/neon-demo.js')}}"></script>
	@yield('scripts')

</body>
</html>