@extends('admin')



@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">Add User</div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">
		
		<form action="{{ route('users.store') }}" method="POST" role="form">
			{{ csrf_field() }}

			<div class="row">
				<div class="col-md-6">

					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" value="{{ old('email') }}" name="email" class="form-control" id="email" placeholder="email">
					</div>

					<div class="form-group">
						<label for="first_name">First Name</label>
						<input type="text" value="{{ old('first_name') }}" name="first_name" class="form-control" id="first_name" placeholder="first name">
					</div>

					<div class="form-group">
						<label for="last_name">Last Name</label>
						<input type="text" value="{{ old('last_name') }}" name="last_name" class="form-control" id="last_name" placeholder="last name">
					</div>

					<div class="form-group">
						<label for="username">Middle Name</label>
						<input type="text" value="{{ old('middle_name') }}" name="middle_name" class="form-control" id="middle_name" placeholder="Middle name">
					</div>

				</div>
				<div class="col-md-6">

					<div class="form-group">
						<label for="new_password">Password</label>
						<input type="password" name="password" class="form-control" id="new_password" placeholder="Password">
					</div>

					<div class="form-group">
						<label for="password_confirmation">Repeat Password</label>
						<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Repeat Password">
					</div>

					<label for="user_role">User Role</label>

					@if( $roles )
					@foreach( $roles as $role )
					
					@if( $role->name == 'admin' )
						<div class="form-group">
							<div class="checkbox i-checks">
								<label> <input type="checkbox" name="user_role[]" value="{{ $role->id }}"><i></i> {{ ucfirst( $role->name ) }} </label>
							</div>
						</div>
					@endif
					
					@endforeach
					@endif
					
					<button type="submit" class="btn btn-primary">Save User <i class="fa fa-save"></i></button>
				</div>
			</div>
		
		</form>
		
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('users.index') }}" class="btn btn-white">
	Back <i class="fa fa-chevron-left"></i> 
</a>
</div>
@endsection


@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@endsection