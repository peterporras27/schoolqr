@extends('admin')


@section('content')


<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">User Information</div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">
		
		<form action="{{ route( 'users.update', $user->id ) }}" method="POST" role="form">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PUT">
			<div class="row">
				<div class="col-md-6">

					<div class="form-group">
						<label for="visitor">Visitor?&nbsp;</label>
						<div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default{{ $user->visitor ? ' active':'' }}">
                                <input type="radio" name="visitor" value="yes" id="option1"{{ $user->visitor ? ' checked':'' }}> Yes
                            </label>
                            <label class="btn btn-default{{ $user->visitor ? '':' active' }}">
                                <input type="radio" name="visitor" value="no" id="option2"{{ $user->visitor ? '':' checked' }}> No
                            </label>
                        </div>
					</div>

					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" value="{{ $user->email }}" name="email" class="form-control" id="email" placeholder="email">
					</div>

					<div class="form-group">
						<label for="first_name">First Name</label>
						<input type="text" value="{{ $user->first_name }}" name="first_name" class="form-control" id="first_name" placeholder="first name">
					</div>

					<div class="form-group">
						<label for="last_name">Last Name</label>
						<input type="text" value="{{ $user->last_name }}" name="last_name" class="form-control" id="last_name" placeholder="last name">
					</div>

					<div class="form-group">
						<label for="middle_name">Middle Name</label>
						<input type="text" value="{{ $user->middle_name }}" name="middle_name" class="form-control" id="middle_name" placeholder="Middle name">
					</div>

					<div id="others" style="{{ $user->visitor ? 'display:none;':'' }}">
					
						<div class="form-group">
							<label for="school">School</label>
							<input type="text" value="{{ $user->school }}" name="school" class="form-control" id="school" placeholder="School of Info...">
						</div>

						<div class="form-group">
							<label for="id_number">ID Number</label>
							<input type="text" value="{{ $user->id_number }}" name="id_number" class="form-control" id="id_number" placeholder="">
						</div>

						<div class="form-group">
							<label for="notify_person">Incase Of Emergency Notify:</label>
							<input type="text" value="{{ $user->notify_person }}" name="notify_person" class="form-control" id="notify_person" placeholder="Name">
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="notify_address">Address</label>
									<input type="text" value="{{ $user->notify_address }}" name="notify_address" class="form-control" id="notify_address" placeholder="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="notify_phone">Phone Number</label>
									<input type="number" value="{{ $user->notify_phone }}" name="notify_phone" class="form-control" id="notify_phone" placeholder="">
								</div>
							</div>
						</div>

					</div>
					

					<hr>
					<button type="submit" class="btn btn-primary btn-icon">
						Save <i class="fa fa-save"></i>
					</button>

				</div>
				<div class="col-md-6">

					<div class="form-group{{ $errors->has('vehicle_type') ? ' has-error' : '' }}">
                        <label for="first_name" class=" control-label">Vehicle type:</label>
                        <div class="">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default{{ $user->vehicle_type == 'Car' ? ' active':'' }}">
                                    <input type="radio" name="vehicle_type" value="Car"{{ $user->vehicle_type == 'Car' ? ' checked':'' }}> Car
                                </label>
                                <label class="btn btn-default{{ $user->vehicle_type == 'Motorcycle' ? ' active':'' }}">
                                    <input type="radio" name="vehicle_type" value="Motorcycle"{{ $user->vehicle_type == 'Motorcycles' ? ' checked':'' }}> Motorcycle
                                </label>
                            </div>
                            @if ($errors->has('vehicle_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('vehicle_type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('vehicle_brand') ? ' has-error' : '' }}">
                        <label for="vehicle_brand" class=" control-label">Vehicle Brand:</label>
                        <div class="">
                            <input id="vehicle_brand" type="text" class="form-control" name="vehicle_brand" value="{{ $user->vehicle_brand }}" placeholder="Toyota" required>
                            @if ($errors->has('vehicle_brand'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('vehicle_brand') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('vehicle_model') ? ' has-error' : '' }}">
                        <label for="vehicle_model" class=" control-label">Vehicle Model:</label>
                        <div class="">
                            <input id="vehicle_model" type="text" class="form-control" name="vehicle_model" value="{{ $user->vehicle_model }}" placeholder="Toyota Wigo..." required>
                            @if ($errors->has('vehicle_model'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('vehicle_model') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                        <label for="plate_number" class=" control-label">Plate Number:</label>
                        <div class="">
                            <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ $user->plate_number }}" required>
                            @if ($errors->has('plate_number'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('plate_number') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

					<div class="form-group">
						<label for="last_name">Reset Password</label>
						<p><i>Users can request for a password reset upon clicking on <strong>forgot password</strong> in the login page. Password reset link will be sent to the user's email account.</i></p>
					</div>

					<label for="user_role">User Role</label>
					
					@if( $roles )
					@foreach( $roles as $role )

					<div class="form-group">
						<div class="checkbox i-checks">
							<label> <input type="checkbox" {{ in_array($role->id, $user_roles) ? 'checked':'' }} name="user_role[]" value="{{ $role->id }}"><i></i> {{ ucfirst( $role->name ) }} </label>
						</div>
					</div>
					
					@endforeach
					@endif
					
				</div>
			</div>
		
		</form>

	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('users.index') }}" class="btn btn-white">
	 <i class="fa fa-chevron-left"></i> Go Back
</a>
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            $('[name="visitor"]').change(function(){
		        var isVisitor = $(this).val();

		        if ( isVisitor == 'yes' ) {
		            $('#others').fadeOut();
		        } else {
		            $('#others').fadeIn();
		        }
		    });
        });
    </script>
@endsection

